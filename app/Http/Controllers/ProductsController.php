<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product as Product;

class ProductsController extends Controller
{
    public function index()
    {

        $data['products'] = Product::all();

        return view('products.show_all', $data);

    }

    public function products(Product $product){

        $data['products'] = $product;
        return view('products.show')->with($data);

    }



    //Delete product
    public function destroy(Product $product){

        $product->delete();
        return redirect('/products');

    }

    //Create product
    public function create(){

        return view('products.create');

    }

    public function store(){

        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
        ]);

        Product::create(request(['title', 'alias', 'price', 'description']));

        return redirect('/products');

    }

    //Edit page
    public function edit(Product $product){

        return view('products.edit', compact('product'));

    }

    public function update(Product $product){

        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
        ]);

        $product->update(request(['title', 'alias', 'price', 'description']));

        return redirect('/products');

    }
}
