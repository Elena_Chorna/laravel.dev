<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    public function index()
    {

        $data['orders'] = DB::table('orders')->get();

        return view('orders.show_all', $data);

    }

}
