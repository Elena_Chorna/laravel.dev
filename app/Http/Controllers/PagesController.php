<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Page as Page;

class PagesController extends Controller
{

    //Show all pages
    public function index()
    {

        $data['pages'] = Page::all();
        return view('pages.show_all', $data);

    }

    //Show page
    public function pages(Page $page)
    {

        $data['pages'] = $page;
        return view('pages.show')->with($data);

    }

    //Delete page
    public function destroy(Page $page){

        $page->delete();
        return redirect('/pages');

    }

    //Create page
    public function create(){

        return view('pages.create');

    }

    public function store(){

        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);

        Page::create(request(['title', 'alias', 'intro', 'content']));

        return redirect('/pages');

    }

    //Edit page
    public function edit(Page $page){

        return view('pages.edit', compact('page'));

    }

    public function update(Page $page){

        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);

        $page->update(request()->all());

        return redirect('/pages');

    }
}
