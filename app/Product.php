<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title', 'alias', 'price', 'description'];
    public $timestamps = false;
    public function getRouteKeyName()
    {
        return 'alias';
    }
}
