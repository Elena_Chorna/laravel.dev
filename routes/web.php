<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/products',"ProductsController@index");
Route::get('/products/create',"ProductsController@create");
Route::get('/products/{product}/edit',"ProductsController@edit");
Route::post('/products/{product}',"ProductsController@update");
Route::put('/products', "ProductsController@store");
Route::get('/products/{product}',"ProductsController@products");
Route::delete('/products/{product}',"ProductsController@destroy");


Route::get('/orders',"OrdersController@index");


Route::get('/pages',"PagesController@index");
Route::get('/pages/create',"PagesController@create");
Route::get('/pages/{page}/edit',"PagesController@edit");
Route::post('/pages/{page}',"PagesController@update");
Route::put('/pages', "PagesController@store");
Route::get('/pages/{page}',"PagesController@pages");
Route::delete('/pages/{page}',"PagesController@destroy");