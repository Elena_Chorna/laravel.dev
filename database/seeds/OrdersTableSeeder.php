<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            array(
                'customer_name' => 'John',
                'email' => 'john@com.com',
                'phone' =>  '12 12 12',
                'feedback' =>  'In hac habitasse platea dictumst. Mauris mattis, metus eu molestie sagittis, nibh purus ultricies diam, dignissim laoreet magna lectus sit amet lacus. Vestibulum id purus mi. Donec mollis, massa in blandit tristique, nunc urna iaculis elit, vitae elementum tellus nisi non elit.'
            ),
            array(
                'customer_name' => 'Anna',
                'email' => 'anna@com.com',
                'phone' =>  '52 32 82',
                'feedback' =>  'Lectus sit amet lacus. Vestibulum id purus mi. Donec mollis, massa in blandit tristique, nunc urna iaculis elit, vitae elementum tellus nisi non elit.'
            )
        ]);
    }
}
