<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            array(
                'title' => 'Proin laoreet magna',
                'alias' => 'proin-laoreet-magna',
                'intro' =>  'Proin laoreet magna ac tellus pretium, vel laoreet nibh dapibus. Cras pharetra tempor enim, sed pellentesque mauris fermentum sit amet. Cras eros ex, commodo eget magna nec, aliquet pretium lectus. ',
                'content' => 'Proin laoreet magna ac tellus pretium, vel laoreet nibh dapibus. Cras pharetra tempor enim, sed pellentesque mauris fermentum sit amet. Cras eros ex, commodo eget magna nec, aliquet pretium lectus. Donec dui erat, semper nec quam ac, varius malesuada purus. Phasellus turpis ante, dapibus non gravida ac, consectetur eget purus. Nunc lobortis augue sit amet nunc molestie rhoncus. Phasellus vitae tellus mauris. Nunc eros mauris, tincidunt vitae fringilla in, congue ut diam. Donec eget massa consectetur, vulputate tortor in, condimentum urna. Donec fermentum purus vestibulum magna porttitor, ut consectetur augue malesuada. Quisque in risus a augue tincidunt varius sed in dui. Quisque metus nisi, sodales eu arcu et, mattis cursus sem. In ultrices porta urna, vel vestibulum magna maximus sed.'
            ),
            array(
                'title' => 'Aenean sit',
                'alias' => 'aenean-sit',
                'intro' =>  'Aenean sit amet elementum eros, a convallis nulla. Ut vel eros a mi dictum semper id non sapien. Proin laoreet magna ac tellus pretium, vel laoreet nibh dapibus. Cras pharetra tempor enim, sed pellentesque mauris fermentum sit amet. Cras eros ex, commodo eget magna nec, aliquet pretium lectus. ',
                'content' => 'Aenean sit amet elementum eros, a convallis nulla. Ut vel eros a mi dictum semper id non sapien. Proin laoreet magna ac tellus pretium, vel laoreet nibh dapibus. Cras pharetra tempor enim, sed pellentesque mauris fermentum sit amet. Cras eros ex, commodo eget magna nec, aliquet pretium lectus. Donec dui erat, semper nec quam ac, varius malesuada purus. Phasellus turpis ante, dapibus non gravida ac, consectetur eget purus. Nunc lobortis augue sit amet nunc molestie rhoncus. Phasellus vitae tellus mauris. Nunc eros mauris, tincidunt vitae fringilla in, congue ut diam. Donec eget massa consectetur, vulputate tortor in, condimentum urna. Donec fermentum purus vestibulum magna porttitor, ut consectetur augue malesuada. Quisque in risus a augue tincidunt varius sed in dui. Quisque metus nisi, sodales eu arcu et, mattis cursus sem. In ultrices porta urna, vel vestibulum magna maximus sed.'
            ),
            array(
                'title' => 'In non luctus metus',
                'alias' => 'in-non-luctus',
                'intro' =>  'Proin laoreet magna ac tellus pretium, vel laoreet nibh dapibus. Cras pharetra tempor enim, sed pellentesque mauris fermentum sit amet. Cras eros ex, commodo eget magna nec, aliquet pretium lectus. ',
                'content' => 'Proin laoreet magna ac tellus pretium, vel laoreet nibh dapibus. Cras pharetra tempor enim, sed pellentesque mauris fermentum sit amet. Cras eros ex, commodo eget magna nec, aliquet pretium lectus. Donec dui erat, semper nec quam ac, varius malesuada purus. Phasellus turpis ante, dapibus non gravida ac, consectetur eget purus. Nunc lobortis augue sit amet nunc molestie rhoncus. Phasellus vitae tellus mauris. Nunc eros mauris, tincidunt vitae fringilla in, congue ut diam. Donec eget massa consectetur, vulputate tortor in, condimentum urna. Donec fermentum purus vestibulum magna porttitor, ut consectetur augue malesuada. Quisque in risus a augue tincidunt varius sed in dui. Quisque metus nisi, sodales eu arcu et, mattis cursus sem. In ultrices porta urna, vel vestibulum magna maximus sed.'
            )
        ]);
    }
}
