<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            array(
                'title' => 'Bread',
                'alias' => 'bread',
                'price' =>  12,
                'description' =>  'Praesent eu magna cursus neque finibus blandit in sed sem. Donec ut mi nisl. Ut tempus volutpat tellus. Suspendisse vel metus et justo finibus sagittis. In hac habitasse platea dictumst. Mauris mattis, metus eu molestie sagittis, nibh purus ultricies diam, dignissim laoreet magna lectus sit amet lacus. Vestibulum id purus mi. Donec mollis, massa in blandit tristique, nunc urna iaculis elit, vitae elementum tellus nisi non elit.'
            ),
            array(
                'title' => 'Meat',
                'alias' => 'meat',
                'price' =>  52.3,
                'description' =>  'Quisque viverra, eros at blandit dapibus, justo metus volutpat orci, sed semper sem dolor nec purus. Morbi sit amet nibh dui. Praesent non est nulla. Donec accumsan neque id pharetra eleifend. Fusce suscipit facilisis nibh et dapibus. Aenean sit amet diam vitae magna interdum vehicula in non diam. Aenean accumsan, tortor et euismod ultricies, nisl risus dictum urna, quis interdum magna diam eu purus.'
            ),
            array(
                'title' => 'Water',
                'alias' => 'water',
                'price' =>  20,
                'description' =>  'Praesent eu magna cursus neque finibus blandit in sed sem. Donec ut mi nisl. Ut tempus volutpat tellus. Suspendisse vel metus et justo finibus sagittis. In hac habitasse platea dictumst. Mauris mattis, metus eu molestie sagittis, nibh purus ultricies diam, dignissim laoreet magna lectus sit amet lacus. Vestibulum id purus mi. Donec mollis, massa in blandit tristique, nunc urna iaculis elit, vitae elementum tellus nisi non elit.'
            ),
            array(
                'title' => 'Apples',
                'alias' => 'apples',
                'price' =>  12.8,
                'description' =>  'Quisque viverra, eros at blandit dapibus, justo metus volutpat orci, sed semper sem dolor nec purus. Morbi sit amet nibh dui. Praesent non est nulla. Donec accumsan neque id pharetra eleifend. Fusce suscipit facilisis nibh et dapibus. Aenean sit amet diam vitae magna interdum vehicula in non diam. Aenean accumsan, tortor et euismod ultricies, nisl risus dictum urna, quis interdum magna diam eu purus.'
            )
        ]);
    }
}
