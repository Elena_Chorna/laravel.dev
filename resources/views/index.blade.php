@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($posts AS $post)
            <div class="col-md-12">
                <h2>{{$post->title}}</h2>
                <p>{{$post->intro}}</p>
                <p><a class="btn btn-default" href="/posts/{{$post->id}}" role="button">Читать далее »</a></p>
            </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Hello, PHP!</h1>
    </div>
@endsection