@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <form method="post" action="/pages">

                <input type="hidden" name="_method" value="PUT">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input name="title" type="text" id="title" class="form-control">
                </div>

                <div class="form-group">
                    <label for="alias">Alias:</label>
                    <input name="alias" type="text" id="alias" class="form-control">
                </div>

                <div class="form-group">
                    <label for="intro">Intro:</label>
                    <input name="intro" type="text" id="intro" class="form-control">
                </div>

                <div class="form-group">
                    <label for="body">Content:</label>
                    <textarea name="content" id="content" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>

            @include('layouts.formError')

        </div>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h2>Add new page</h2>
    </div>
@endsection