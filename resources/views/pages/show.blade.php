@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p>
                {{$pages->content}}
            </p>
        </div>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h2>{{$pages->title}}</h2>
    </div>
@endsection