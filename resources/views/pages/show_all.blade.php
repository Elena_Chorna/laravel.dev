@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($pages AS $page)
            <div class="col-md-4">
                <h2>{{$page->title}}</h2>
                <p>{{$page->intro}}</p>
                <p><a class="btn btn-default" href="/pages/{{$page->alias}}" role="button">Read more »</a></p>
                <p><a class="btn btn-default" href="/pages/{{$page->alias}}/edit" role="button">Edit »</a></p>
                <p>
                    <form action="/pages/{{$page->alias}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" value="Delete »" class="btn btn-danger">
                    </form>
                </p>
            </div>
        @endforeach
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h3>Add new page <a class="btn btn-default" href="/pages/create" role="button">Create »</a></h3>
        </div>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Pages</h1>
    </div>
@endsection