@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($products AS $product)
            <div class="col-md-4">
                <h2>{{$product->title}}</h2>
                <p><b>PRICE</b> {{$product->price}} UAH</p>
                <p><a class="btn btn-default" href="/products/{{$product->alias}}" role="button">Read more »</a></p>
                <p><a class="btn btn-default" href="/products/{{$product->alias}}/edit" role="button">Edit »</a></p>
                <p>
                <form action="/products/{{$product->alias}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Delete »" class="btn btn-danger">
                </form>
                </p>
            </div>
        @endforeach
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h3>Add new product <a class="btn btn-default" href="/products/create" role="button">Create »</a></h3>
        </div>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Our Products</h1>
    </div>
@endsection