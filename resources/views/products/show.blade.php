@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p>
               <b>PRICE</b> {{$products->price}} UAH
            </p>
            <p>
                {{$products->description}}
            </p>
        </div>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h2>{{$products->title}}</h2>
    </div>
@endsection