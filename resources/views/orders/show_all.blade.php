@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Customer Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Feedback</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders AS $order)
                    <tr>
                        <td scope="row">{{$order->id}}</td>
                        <td scope="row"><h4>{{$order->customer_name}}</h4></td>
                        <td scope="row">{{$order->email}}</td>
                        <td scope="row">{{$order->phone}}</td>
                        <td>{{$order->feedback}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Orders</h1>
    </div>
@endsection